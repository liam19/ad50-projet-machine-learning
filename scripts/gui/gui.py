import tkinter as tk
from tkinter import ttk
import numpy as np
import pandas as pd
from analysis.statistics import get_statistics
from analysis.final_dataset_analysis import get_processed_final_dataset
from sklearn.preprocessing import StandardScaler
from datetime import date
from keras.models import load_model
from joblib import dump, load
from analysis.jours_ferie_analysis import get_processed_jours_ferie_dataset

def handle_focus_in(event, ent, txtvar):
    if txtvar.get() == "MM/DD/YYYY":
        ent.delete(0, tk.END)
        ent.config(fg='black')

def handle_focus_out(event, ent, txtvar):
    if not txtvar.get():
        ent.delete(0, tk.END)
        ent.config(fg='grey')
        ent.insert(0, "MM/DD/YYYY")

def date_to_iso(date_str):
    return date_str.split("/")[2] + "-" + date_str.split("/")[0] + "-" + date_str.split("/")[1]

def date_to_week_number(date_str):
    ISO_str = date_to_iso(date_str)
    ISO = date.fromisoformat(ISO_str)
    return ISO.isocalendar()[1]

class GUI:        
    def __init__(self):
        self.final_dataset = get_processed_final_dataset()
        self.jours_ferie_dataset = get_processed_jours_ferie_dataset()
        
        entrypadx = 60
        self.root = tk.Tk()
        self.root.title("Intervention prediction")
        self.root.geometry("1050x500")
        self.root.resizable(False, False)
        self.root.iconbitmap('icon.ico')

        self.upFrame = tk.Frame(self.root)
        self.dateFrame = tk.LabelFrame(self.root, text = "Date")
        self.weatherFrame = tk.LabelFrame(self.root, text = "Weather")
        self.operationTypeFrame = tk.LabelFrame(self.root, text = "Operation type")
        self.lowFrame = tk.Frame(self.root)

        self.title = tk.Label(self.upFrame, text = "Intervention prediction", font = ("Arial", 20, "bold")).grid(row = 0, column = 0, pady = 40)

        wolbl1 = tk.Label(self.dateFrame, text = "Week of:").grid(row = 0, column = 0, padx = entrypadx)
        self.weekof = tk.StringVar()
        self.woent1 = tk.Entry(self.dateFrame, textvariable = self.weekof, fg='grey')
        self.woent1.insert(0, "MM/DD/YYYY")
        self.woent1.bind("<FocusIn>", lambda event, ent = self.woent1, txtvar = self.weekof: handle_focus_in(event, ent, txtvar))
        self.woent1.bind("<FocusOut>", lambda event, ent = self.woent1, txtvar = self.weekof: handle_focus_out(event, ent, txtvar))    
        self.woent1.grid(row = 0, column = 1, pady = 0)
        wolbl2 = tk.Label(self.dateFrame, text = "To :").grid(row = 0, column = 2, padx = entrypadx)
        self.weekto = tk.StringVar()
        self.woent2 = tk.Entry(self.dateFrame, textvariable = self.weekto, fg='grey')
        self.woent2.insert(0, "MM/DD/YYYY")
        self.woent2.bind("<FocusIn>", lambda event, ent = self.woent2, txtvar = self.weekto: handle_focus_in(event, ent, txtvar))
        self.woent2.bind("<FocusOut>", lambda event, ent = self.woent2, txtvar = self.weekto: handle_focus_out(event, ent, txtvar))    
        self.woent2.grid(row = 0, column = 3, pady = 15)

        dtlbl1 = tk.Label(self.weatherFrame, text = "Rain:").grid(row = 0, column = 0, padx = entrypadx)
        dtlbl2 = tk.Label(self.weatherFrame, text = "Snow:").grid(row = 0, column = 1, padx = entrypadx)
        dtlbl4 = tk.Label(self.weatherFrame, text = "Sun:").grid(row = 0, column = 2, padx = entrypadx)
        dtlbl5 = tk.Label(self.weatherFrame, text = "Wind:").grid(row = 0, column = 3, padx = entrypadx)
        dtlbl6 = tk.Label(self.weatherFrame, text = "Temperature:").grid(row = 0, column = 4, padx = entrypadx)
        #dtlbl4 = tk.Label(self.weatherFrame, text = "data 4").grid(row = 0, column = 3, padx = entrypadx)
        #dtlbl5 = tk.Label(self.weatherFrame, text = "data 5").grid(row = 0, column = 4, padx = entrypadx)

        weatherList=["(Choose)", "Snow", "Slightly rainy", "Rainy", "Very rainy", "Fog", "Sunny", "Very sunny"]
        
        self.rainList=["(Choose)", "No rain (< 1 mm)", "Slighty rainy (1-3 mm)", "Rainy (3-6 mm)", "Very rainy (6 - 10 mm)", "Very very rainy (> 10 mm)", "END OF THE WORLD"]
        self.comboRain = ttk.Combobox(self.weatherFrame, values=self.rainList)
        self.comboRain.current(0)
        self.comboRain.grid(row = 1, column = 0, pady = 20)

        self.snowList=["(Choose)", "No snow", "Slighty snowy (< 0.5 cm)", "Moderately snowy (0.5-2 cm)", "Very snowy > 2 cm", "END OF THE WORLD"]
        self.comboSnow = ttk.Combobox(self.weatherFrame, values=self.snowList)
        self.comboSnow.current(0)
        self.comboSnow.grid(row = 1, column = 1, pady = 20)

        self.sunList = ["(Choose)", "Not very suny", "A bit sunny", "Sunny", "Very sunny", "END OF THE WORLD"]
        self.comboSun = ttk.Combobox(self.weatherFrame, values=self.sunList)
        self.comboSun.current(0)
        self.comboSun.grid(row = 1, column = 2, pady = 20)

        self.windList=["(Choose)", "No wind", "Little wind", "Average wind", "Hard wind", "END OF THE WORLD"]
        self.comboWind = ttk.Combobox(self.weatherFrame, values=self.windList)
        self.comboWind.current(0)
        self.comboWind.grid(row = 1, column = 3, pady = 20)

        #tempList=["(Choose)", "Very cold (< 0)", "Cold (0-10)", "Normal(10-20)", "Warm (20-30)", "HELL (>30)"]
        self.temperature = tk.StringVar()
        self.entryTemp = tk.Entry(self.weatherFrame, textvariable = self.temperature)
        self.entryTemp.grid(row = 1, column = 4, pady = 20)
        
##        self.var1 = tk.StringVar()
##        self.entr1 = tk.Entry(self.weatherFrame, textvariable = self.var1).grid(row = 1, column = 0, pady = 20)
##        self.var2 = tk.StringVar()
##        self.entr2 = tk.Entry(self.weatherFrame, textvariable = self.var2).grid(row = 1, column = 1, pady = 20)
##        self.var3 = tk.StringVar()
##        self.entr3 = tk.Entry(self.weatherFrame, textvariable = self.var3).grid(row = 1, column = 2, pady = 20)
##        self.var4 = tk.StringVar()
##        self.entr4 = tk.Entry(self.weatherFrame, textvariable = self.var4).grid(row = 1, column = 3, pady = 20)
##        self.var5 = tk.StringVar()
##        self.entr5 = tk.Entry(self.weatherFrame, textvariable = self.var5).grid(row = 1, column = 4, pady = 20)

        self.otlbl = tk.Label(self.operationTypeFrame, text = "Operation type:").grid(row = 0, column = 0, padx = entrypadx/2)
        self.operationList=["(Choose)", "ALL", "SUAP", "ACCI", "INCN", "INCU", "AUTR"]
        self.comboOperation = ttk.Combobox(self.operationTypeFrame, values=self.operationList)
        self.comboOperation.current(0)
        self.comboOperation.grid(row = 0, column = 1, padx = 10, pady = 10)

        self.buttonPredict = tk.Button(self.lowFrame, command = self.predict, text = "Predict", font = ("Arial", 16, "normal"))
        self.buttonPredict.grid(row = 0, column = 0, pady = 20)

        self.upFrame.grid(row = 0, column = 0)
        self.dateFrame.grid(row = 1, column = 0, padx = 100, pady = 20)
        self.weatherFrame.grid(row = 2, column = 0, padx = 50)
        self.operationTypeFrame.grid(row = 3, column = 0, pady = 20)
        self.lowFrame.grid(row = 4, column = 0)
        self.root.mainloop()

    def predict(self):
        codes_insee = pd.read_csv('../datasets/insee_91.csv').to_numpy()
        s_rain = get_statistics("rain")[self.rainList.index(self.comboRain.get()) - 1]
        s_snow = get_statistics("snow")[self.snowList.index(self.comboSnow.get()) - 1]
        s_sun = get_statistics("sun")[self.sunList.index(self.comboSun.get()) - 1]
        s_wind = get_statistics("wind")[self.windList.index(self.comboWind.get()) - 1]
        s_mean_temperature = float(self.entryTemp.get())
        s_min_temperature = s_mean_temperature - 5
        s_max_temperature = s_mean_temperature + 5

        week_number = date_to_week_number(self.woent1.get())

        suap_pred = 0
        acci_pred = 0
        incn_pred = 0
        incu_pred = 0
        autr_pred = 0

        if self.comboOperation.get() == "SUAP" or self.comboOperation.get() == "ALL":
            suap_model = load("../models/SUAP_model.model")

            
            suapx = self.final_dataset["SUAP"][0]
            sorted_suap = suapx[suapx["ope_semaine"] == week_number]
            
            inc100_flu = sorted_suap["inc100_flu"].mean()
            inc100_low_flu = sorted_suap["inc100_low_flu"].mean()
            inc100_up_flu = sorted_suap["inc100_up_flu"].mean()
            
            inc100_diarrhea = sorted_suap["inc100_diarrhea"].mean()
            inc100_low_diarrhea = sorted_suap["inc100_low_diarrhea"].mean()
            inc100_up_diarrhea = sorted_suap["inc100_up_diarrhea"].mean()
            
            inc100_chickenpox = sorted_suap["inc100_chickenpox"].mean()
            inc100_low_chickenpox = sorted_suap["inc100_low_chickenpox"].mean()
            inc100_up_chickenpox = sorted_suap["inc100_up_chickenpox"].mean()

            temp_jf = self.jours_ferie_dataset[self.jours_ferie_dataset["numero_semaine"] == week_number]
            jf_semaine = temp_jf[temp_jf["annee"] == int(self.woent1.get().split("/")[2])]["nombre_jours_feries_semaine"].to_numpy()[0]

            humidity = sorted_suap["humidity"].mean()
            pressure = sorted_suap["pressure"].mean()
            
            scaler = load("../models/SUAP_scaler.pkl")

            for code_insee in codes_insee:
                X = np.array([code_insee,week_number,inc100_flu,inc100_low_flu,inc100_up_flu,inc100_diarrhea,inc100_low_diarrhea,
                              inc100_up_diarrhea,inc100_chickenpox,inc100_low_chickenpox,inc100_up_chickenpox,jf_semaine,
                              s_max_temperature,s_min_temperature,humidity,pressure,s_mean_temperature,s_wind,s_snow,s_sun,s_rain])
                
                X_scaled = scaler.transform(X.reshape(1, -1))

                
                suap_pred = suap_pred + suap_model.predict(X_scaled)

        if self.comboOperation.get() == "INCN" or self.comboOperation.get() == "ALL":
            incn_model = load("../models/INCN_model.model")
            
            incnx = self.final_dataset["INCN"][0]
            sorted_incn = incnx[incnx["ope_semaine"] == week_number]
            
            inc100_flu = sorted_incn["inc100_flu"].mean()
            inc100_low_flu = sorted_incn["inc100_low_flu"].mean()
            inc100_up_flu = sorted_incn["inc100_up_flu"].mean()
            
            inc100_diarrhea = sorted_incn["inc100_diarrhea"].mean()
            inc100_low_diarrhea = sorted_incn["inc100_low_diarrhea"].mean()
            inc100_up_diarrhea = sorted_incn["inc100_up_diarrhea"].mean()
            
            inc100_chickenpox = sorted_incn["inc100_chickenpox"].mean()
            inc100_low_chickenpox = sorted_incn["inc100_low_chickenpox"].mean()
            inc100_up_chickenpox = sorted_incn["inc100_up_chickenpox"].mean()

            temp_jf = self.jours_ferie_dataset[self.jours_ferie_dataset["numero_semaine"] == week_number]
            jf_semaine = temp_jf[temp_jf["annee"] == int(self.woent1.get().split("/")[2])]["nombre_jours_feries_semaine"].to_numpy()[0]

            humidity = sorted_incn["humidity"].mean()
            pressure = sorted_incn["pressure"].mean()

            scaler = load("../models/INCN_scaler.pkl")

            for code_insee in codes_insee:
                X = np.array([code_insee,week_number,inc100_flu,inc100_low_flu,inc100_up_flu,inc100_diarrhea,inc100_low_diarrhea,
                              inc100_up_diarrhea,inc100_chickenpox,inc100_low_chickenpox,inc100_up_chickenpox,jf_semaine,
                              s_max_temperature,s_min_temperature,humidity,pressure,s_mean_temperature,s_wind,s_snow,s_sun,s_rain])
                
                X_scaled = scaler.transform(X.reshape(1, -1))

                
                incn_pred = incn_pred + incn_model.predict(X_scaled)

        if self.comboOperation.get() == "INCU" or self.comboOperation.get() == "ALL":
            incu_model = load("../models/INCU_model.model")
            incux = self.final_dataset["INCU"][0]
            sorted_incu = incux[incux["ope_semaine"] == week_number]
            
            inc100_flu = sorted_incu["inc100_flu"].mean()
            inc100_low_flu = sorted_incu["inc100_low_flu"].mean()
            inc100_up_flu = sorted_incu["inc100_up_flu"].mean()
            
            inc100_diarrhea = sorted_incu["inc100_diarrhea"].mean()
            inc100_low_diarrhea = sorted_incu["inc100_low_diarrhea"].mean()
            inc100_up_diarrhea = sorted_incu["inc100_up_diarrhea"].mean()
            
            inc100_chickenpox = sorted_incu["inc100_chickenpox"].mean()
            inc100_low_chickenpox = sorted_incu["inc100_low_chickenpox"].mean()
            inc100_up_chickenpox = sorted_incu["inc100_up_chickenpox"].mean()

            temp_jf = self.jours_ferie_dataset[self.jours_ferie_dataset["numero_semaine"] == week_number]
            jf_semaine = temp_jf[temp_jf["annee"] == int(self.woent1.get().split("/")[2])]["nombre_jours_feries_semaine"].to_numpy()[0]

            humidity = sorted_incu["humidity"].mean()
            pressure = sorted_incu["pressure"].mean()
            
            scaler = load("../models/INCU_scaler.pkl")

            for code_insee in codes_insee:
                X = np.array([code_insee,week_number,inc100_flu,inc100_low_flu,inc100_up_flu,inc100_diarrhea,inc100_low_diarrhea,
                              inc100_up_diarrhea,inc100_chickenpox,inc100_low_chickenpox,inc100_up_chickenpox,jf_semaine,
                              s_max_temperature,s_min_temperature,humidity,pressure,s_mean_temperature,s_wind,s_snow,s_sun,s_rain])
                
                X_scaled = scaler.transform(X.reshape(1, -1))

                
                incu_pred = incu_pred + incu_model.predict(X_scaled)

        if self.comboOperation.get() == "ACCI" or self.comboOperation.get() == "ALL":
            acci_model = load("../models/ACCI_model.model")
            accix = self.final_dataset["ACCI"][0]
            sorted_acci = accix[accix["ope_semaine"] == week_number]
            
            inc100_flu = sorted_acci["inc100_flu"].mean()
            inc100_low_flu = sorted_acci["inc100_low_flu"].mean()
            inc100_up_flu = sorted_acci["inc100_up_flu"].mean()
            
            inc100_diarrhea = sorted_acci["inc100_diarrhea"].mean()
            inc100_low_diarrhea = sorted_acci["inc100_low_diarrhea"].mean()
            inc100_up_diarrhea = sorted_acci["inc100_up_diarrhea"].mean()
            
            inc100_chickenpox = sorted_acci["inc100_chickenpox"].mean()
            inc100_low_chickenpox = sorted_acci["inc100_low_chickenpox"].mean()
            inc100_up_chickenpox = sorted_acci["inc100_up_chickenpox"].mean()

            temp_jf = self.jours_ferie_dataset[self.jours_ferie_dataset["numero_semaine"] == week_number]
            jf_semaine = temp_jf[temp_jf["annee"] == int(self.woent1.get().split("/")[2])]["nombre_jours_feries_semaine"].to_numpy()[0]

            humidity = sorted_acci["humidity"].mean()
            pressure = sorted_acci["pressure"].mean()

            scaler = load("../models/ACCI_scaler.pkl")

            for code_insee in codes_insee:
                X = np.array([code_insee,week_number,inc100_flu,inc100_low_flu,inc100_up_flu,inc100_diarrhea,inc100_low_diarrhea,
                              inc100_up_diarrhea,inc100_chickenpox,inc100_low_chickenpox,inc100_up_chickenpox,jf_semaine,
                              s_max_temperature,s_min_temperature,humidity,pressure,s_mean_temperature,s_wind,s_snow,s_sun,s_rain])
                
                X_scaled = scaler.transform(X.reshape(1, -1))

                
                acci_pred = acci_pred + acci_model.predict(X_scaled)

        if self.comboOperation.get() == "AUTR" or self.comboOperation.get() == "ALL":
            autr_model = load("../models/AUTR_model.model")
            autrx = self.final_dataset["AUTR"][0]
            sorted_autr = autrx[autrx["ope_semaine"] == week_number]
            
            inc100_flu = sorted_autr["inc100_flu"].mean()
            inc100_low_flu = sorted_autr["inc100_low_flu"].mean()
            inc100_up_flu = sorted_autr["inc100_up_flu"].mean()
            
            inc100_diarrhea = sorted_autr["inc100_diarrhea"].mean()
            inc100_low_diarrhea = sorted_autr["inc100_low_diarrhea"].mean()
            inc100_up_diarrhea = sorted_autr["inc100_up_diarrhea"].mean()
            
            inc100_chickenpox = sorted_autr["inc100_chickenpox"].mean()
            inc100_low_chickenpox = sorted_autr["inc100_low_chickenpox"].mean()
            inc100_up_chickenpox = sorted_autr["inc100_up_chickenpox"].mean()

            temp_jf = self.jours_ferie_dataset[self.jours_ferie_dataset["numero_semaine"] == week_number]
            jf_semaine = temp_jf[temp_jf["annee"] == int(self.woent1.get().split("/")[2])]["nombre_jours_feries_semaine"].to_numpy()[0]

            humidity = sorted_autr["humidity"].mean()
            pressure = sorted_autr["pressure"].mean()
            
            scaler = load("../models/AUTR_scaler.pkl")

            for code_insee in codes_insee:
                X = np.array([code_insee,week_number,inc100_flu,inc100_low_flu,inc100_up_flu,inc100_diarrhea,inc100_low_diarrhea,
                              inc100_up_diarrhea,inc100_chickenpox,inc100_low_chickenpox,inc100_up_chickenpox,jf_semaine,
                              s_max_temperature,s_min_temperature,humidity,pressure,s_mean_temperature,s_wind,s_snow,s_sun,s_rain])
                
                X_scaled = scaler.transform(X.reshape(1, -1))

                
                autr_pred = autr_pred + autr_model.predict(X_scaled)


        if self.comboOperation.get() == "SUAP":
            print("SUAP operations: " + str(round(suap_pred[0])))
        elif self.comboOperation.get() == "ACCI":
            print("ACCI operations: " + str(acci_pred[0]))
        elif self.comboOperation.get() == "INCN":
            print("INCN operations: " + str(incn_pred[0]))
        elif self.comboOperation.get() == "INCU":
            print("INCU operations: " + str(incu_pred[0]))
        elif self.comboOperation.get() == "SUAP":
            print("AUTR operations: " + str(autr_pred[0]))
        elif self.comboOperation.get() == "ALL":
            print("SUAP operations: " + str(round(suap_pred[0])))
            print("ACCI operations: " + str(acci_pred[0]))
            print("INCN operations: " + str(incn_pred[0]))
            print("INCU operations: " + str(incu_pred[0]))
            print("AUTR operations: " + str(autr_pred[0]))
            print("Total operations: " + str((round(suap_pred[0]) + acci_pred[0] + incn_pred[0] + incu_pred[0] + autr_pred[0])))
            
            

