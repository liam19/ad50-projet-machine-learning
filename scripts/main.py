from os import path

from gui.gui import GUI

if not path.exists("../models/SUAP_model.model"):
    print("First launch, training model...")
    from analysis import final_dataset_analysis
    from analysis.model_builder import train_test_model
    from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier, AdaBoostClassifier

    sdis_per_cat = final_dataset_analysis.get_processed_final_dataset()
    scores = {}

    print("Train RandomForestRegressor for SUAP")
    # RandomForestRegressor for SUAP
    scores['SUAP'] = train_test_model(sdis_per_cat, 'SUAP', RandomForestRegressor())

    # for the other datasets, use AdaBoostClassifier
    for category in ('ACCI', 'INCN', 'INCU', 'AUTR'):
        print("Train AdaBoostClassifier for " + category)
        scores[category] = train_test_model(sdis_per_cat, category, AdaBoostClassifier())

    print('R² scores for each model: ', scores)

gui = GUI()
