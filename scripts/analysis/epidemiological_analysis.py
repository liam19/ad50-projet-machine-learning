# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 13:18:23 2020

@author: anais
"""

# =============================================================================
# IMPORTS
# =============================================================================

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.compose import ColumnTransformer

# =============================================================================
# COLLECTION
# =============================================================================
def do_epidemiological_dataset_collection(filepath):
    epidemiological_dataset = pd.read_csv(filepath, sep=',')
    
    # General transformations/optimizations ====================================
    # We seperate week and year
    epidemiological_dataset.insert(1, "year", epidemiological_dataset['week'].floordiv(100), True)
    epidemiological_dataset.insert(2, "weeknumber", epidemiological_dataset['week'].mod(100), True)
    # We keep the data only for the ILE-DE-FRANCE as we study the 91 department
    epidemiological_filtered = epidemiological_dataset[epidemiological_dataset['geo_name']=="ILE-DE-FRANCE"]
    # If we want to study the syndrome on the whole France, 
    # the code line above and uncomment the next ligne
    # epidemiological_filtered = epidemiological_dataset

    return epidemiological_filtered

# =============================================================================
# VISUALIZATION
# =============================================================================

def do_epidemiological_dataset_visualization(epidemiological_filtered, name):
    # Evolution over the weeks of the incidence in 2020 ===========================
    # Observations : seems interesting, we should study larger data ---------------
    # -----------------------------------------------------------------------------
    # We keep the data only for 2020
    epidemiological_filtered2020 = epidemiological_filtered[epidemiological_filtered['year']==2020]

    # Creation of the figure
    X = epidemiological_filtered2020['weeknumber']
    plt.plot(X, epidemiological_filtered2020['inc100'], label="estimated rate incidence", color='black')
    plt.plot(X, epidemiological_filtered2020['inc100_low'], label="lower bound", color='green')
    plt.plot(X, epidemiological_filtered2020['inc100_up'], label="upper bound", color='red')
    plt.legend(loc='upper right')
    plt.title('Rate incidence of ${name} syndrome over the weeks in 2020 in Ile-de-France', fontsize=16, fontweight='bold')
    plt.xlabel("Week number")
    plt.ylabel("Estimated rate incidence\nper 100,000 inhabitants")
    plt.show()

    # Evolution of the total incidence over the years in Ile-de-France ============
    # Observations : not really useful ? indicates we have appxomiately the same incidence over the years
    # -----------------------------------------------------------------------------
    X = []
    Y = []
    for year in epidemiological_filtered['year'].unique():
        X.append(year)
        epidemiological_filtered_for_year = epidemiological_filtered[(epidemiological_filtered['year'] == year)]
        Y.append(epidemiological_filtered_for_year['inc'].sum())

    # Creation of the figure 
    plt.bar(X, Y)
    plt.title('Total number of estimated incidence over the years in Ile-de-France')
    plt.xlabel('Year')
    plt.ylabel('Estimated number of incidence')
    plt.ticklabel_format(style='plain', useOffset=False, axis='both')
    plt.show()

    # Evolution over the weeks of the years of the incidence ======================
    # Observations : demonstrates the tendance on the period with more incidence --
    # -----------------------------------------------------------------------------
    # Construction of the data to show
    X = []
    Y = []
    Y_lower = []
    Y_upper = []
    for weeknumber in sorted(epidemiological_filtered['weeknumber'].unique()):
        X.append(weeknumber)
        epidemiological_filtered_for_weeknumber = epidemiological_filtered[(epidemiological_filtered['weeknumber'] == weeknumber)]
        Y.append(epidemiological_filtered_for_weeknumber['inc100'].mean())
        Y_lower.append(epidemiological_filtered_for_weeknumber['inc100_low'].mean())
        Y_upper.append(epidemiological_filtered_for_weeknumber['inc100_up'].mean())
        
    # Creation of the figure
    N = max(X)
    ind = np.arange(N)    # the x locations for the groups
    width = 0.2           # the width of the bars
    plt.bar(ind, Y, width=0.2, label="estimated rate incidence", color='black', align='center')
    plt.bar(ind-width, Y_lower, width=0.2, label="lower bound", color='green', align='center')
    plt.bar(ind+width, Y_upper, width=0.2, label="upper bound", color='red', align='center')
    plt.legend(loc='upper right')
    plt.title("Average rate incidence of ${name} syndrome over the weeks in Ile-de-France", fontsize=16, fontweight='bold')
    plt.xlabel("Week number")
    plt.ylabel("Average rate incidence\nper 100,000 inhabitants")
    plt.show()

# =============================================================================
# SELECTION
# =============================================================================

def do_epidemiological_dataset_selection(dataset, name):
    # We drop the `indicator` column as it contains only one value : 3
    # epidemiological_dataset = epidemiological_dataset.drop(['indicator', 'week', 'inc', 'inc_low', 'inc_up', 'geo_insee', 'geo_name', 'year'], axis=1)
    # X = dataset.drop(['indicator', 'week', 'inc', 'inc_low', 'inc_up', 'inc100_low', 'inc100_up', 'geo_insee', 'geo_name'], axis=1) # IMPORTANT
    # X = X.rename(columns={'inc100': 'inc100_epidemiological'})
    
    # X = dataset.drop(['week', 'geo_insee', 'geo_name'], axis=1)
    X = dataset.drop(['indicator', 'week', 'inc', 'inc_low', 'inc_up', 'geo_insee', 'geo_name'], axis=1) # IMPORTANT
    X.columns = [col + '_' + name for col in X.columns]
        
    # X = pd.DataFrame(data=epidemiological_dataset['weeknumber'], columns=['weeknumber']) # features
    # Y = epidemiological_dataset.drop(columns=['weeknumber']) # target

    return X

# # =============================================================================
# # PREPROCESSING
# # =============================================================================

# # Process on weeknumber (LabelEncoder + OneHotEncoder with ColumnTransform)
# weeknumber_index = 0

# # LabelEncoder
# label_encoder = LabelEncoder()
# X['weeknumber'] = label_encoder.fit_transform(X['weeknumber'])

# onehot_encoder = ColumnTransformer([('onehotencoder', OneHotEncoder(), [weeknumber_index])])

# new_cols = onehot_encoder.fit_transform(X).toarray()
# new_col_names = ['weeknumber_' + str(i) for i in range(1, new_cols.shape[1] + 1)]
# new_cols = pd.DataFrame(data=new_cols, columns=new_col_names)

# X = X.join(new_cols)
# X = X.drop(columns=['weeknumber'])

# # =============================================================================
# # TRANSFORMATION
# # =============================================================================

# scaler = StandardScaler()
# X_scaled = scaler.fit_transform(X)

def get_processed_diarrhea_dataset():
    filepath = '../datasets/epidemiological_diarrhea_syndrome.dataset.csv'
    data = do_epidemiological_dataset_collection(filepath)
    X = do_epidemiological_dataset_selection(data, 'diarrhea')
    # X = do_sdis_dataset_preprocessing(X)
    # X_scaled, sc = do_sdis_dataset_transformation(X)
    
    return X


def get_processed_chickenpox_dataset():
    filepath = '../datasets/epidemiological_chickenpox_syndrome.dataset.csv'
    data = do_epidemiological_dataset_collection(filepath)
    X = do_epidemiological_dataset_selection(data, 'chickenpox')
    # X = do_sdis_dataset_preprocessing(X)
    # X_scaled, sc = do_sdis_dataset_transformation(X)
    
    return X


def get_processed_flu_dataset():
    filepath = '../datasets/epidemiological_flu_syndrome.dataset.csv'
    data = do_epidemiological_dataset_collection(filepath)
    X = do_epidemiological_dataset_selection(data, 'flu')
    # X = do_sdis_dataset_preprocessing(X)
    # X_scaled, sc = do_sdis_dataset_transformation(X)
    
    return X
