from datetime import date
import pandas as pd


def date_to_week_number(ISO_str):
    ISO = date.fromisoformat(ISO_str)
    return ISO.isocalendar()[1]


def add_week_number_column(dataframe):
    date_column = dataframe["date"]
    week_number_column = []
    for data in date_column:
        week_number_column.append(date_to_week_number(data))
    dataframe["numero_semaine"] = week_number_column


def add_holiday_per_week(dataframe):
    week_number_column = dataframe["numero_semaine"]
    holiday_per_week = []
    for i in range(0, len(week_number_column)):
        holiday = 1
        if i != 0:
            if week_number_column[i - 1] == week_number_column[i]:
                n = 1
                while week_number_column[i - n] == week_number_column[i] and i-n >= 0:
                    n += 1
                    holiday += 1
        if i != len(week_number_column) - 1:
            if week_number_column[i + 1] == week_number_column[i]:
                n = 1
                while week_number_column[i + n] == week_number_column[i] and i+n <= len(week_number_column) - 1:
                    n += 1
                    holiday += 1
        holiday_per_week.append(holiday)
    dataframe["nombre_jours_feries_semaine"] = holiday_per_week


def add_no_jours_ferie_in_dataset(X):
    for year in X['annee'].unique():
        for week in range(1, 54):
            if len(X[(X['annee'] == year) & (X['numero_semaine'] == week)]) == 0:
                X.loc[len(X) + 1] = [week, year, 0]
                
    return X


def get_processed_jours_ferie_dataset():
    ENTRY_DATASET_PATH = "../datasets/jours_feries_metropole.csv"
    OUT_DATASET_PATH = "../datasets/jours_feries_metropole.dataset.csv"
    
    dataframe = pd.read_csv(ENTRY_DATASET_PATH)
    add_week_number_column(dataframe)
    add_holiday_per_week(dataframe)
    dataframe = dataframe[["numero_semaine", "annee", "nombre_jours_feries_semaine"]]
    add_no_jours_ferie_in_dataset(dataframe)
    
    return dataframe.iloc[1:, :]
    # dataframe.to_csv(OUT_DATASET_PATH, index = False, header = True)

data = get_processed_jours_ferie_dataset()


