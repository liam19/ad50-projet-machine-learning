from analysis.final_dataset_analysis import get_processed_final_dataset
from analysis.sdis91_analysis import get_processed_sdis_dataset

from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier, AdaBoostClassifier
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score
from sklearn.preprocessing import StandardScaler

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras import metrics

from joblib import dump, load


def train_test_model(sdis_per_cat, category, model):
    X, X_scaled, scaler, y = sdis_per_cat[category]
    X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.33, random_state=42)
    model.fit(X_train, y_train)
    score = model.score(X_test, y_test)

    # Save to file in the current working directory
    model_file = "../models/" + category + '_model.model'
    scaler_file = "../models/" + category + '_scaler.pkl'
    dump(model, model_file)
    dump(scaler, scaler_file)
    
    return score

if __name__ == "__main__":
    sdis_per_cat = get_processed_final_dataset()
    scores = {}

    # RandomForestRegressor for SUAP
    scores['SUAP'] = train_test_model(sdis_per_cat, 'SUAP', RandomForestRegressor())

    # for the other datasets, use AdaBoostClassifier
    for category in ('ACCI', 'INCN', 'INCU', 'AUTR'): 
        scores[category] = train_test_model(sdis_per_cat, category, AdaBoostClassifier())

    print('R² scores for each model: ', scores)



