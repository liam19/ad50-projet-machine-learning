import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

class Model:
    def __init__(self, dataframe):
        self.dataframe = dataframe
        
        nb_dims=dataframe.shape[1]
        target_column_id=2
        columns=range(nb_dims)

        X = dataframe.iloc[:, columns].values
        y = dataframe.iloc[:, target_column_id].values


        # Splitting the dataframe into the Training set and Test set
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

        # Feature Scaling
        from sklearn.preprocessing import StandardScaler
        sc = StandardScaler()
        X_train = sc.fit_transform(X_train)
        X_test = sc.transform(X_test)

        # Part 2 - Now let's make the ANN!

        # Importing the Keras libraries and packages
        import keras
        from keras.models import Sequential
        from keras.layers import Dense

        from keras.wrappers.scikit_learn import KerasClassifier
        from sklearn.model_selection import GridSearchCV
        from keras.models import Sequential
        from keras.layers import Dense

        from keras.optimizers import SGD


        def build_classifier(optimizer,arch):
            classifier = Sequential()
            classifier.add(Dense(units = arch[0], kernel_initializer = 'uniform', activation = 'relu', input_dim = nb_dims))
            for i in range(1,len(arch)):
                classifier.add(Dense(units = arch[i], kernel_initializer = 'uniform', activation = 'relu'))
            classifier.add(Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid'))
            classifier.compile(optimizer = optimizer, loss = 'binary_crossentropy', metrics = ['accuracy'])
            return classifier

        classifier = KerasClassifier(build_fn = build_classifier)
        parameters = {'batch_size': [32],
                      'epochs': [500],
                      'arch':[(6,6),(10,10)],
                      'optimizer': [SGD(lr=0.01)]}
        grid_search = GridSearchCV(estimator = classifier,
                                   param_grid = parameters,
                                   scoring = 'accuracy',
                                   cv = 3)
        grid_search = grid_search.fit(X_train, y_train)
        best_parameters = grid_search.best_params_
        best_accuracy = grid_search.best_score_

        print(best_parameters,best_accuracy)

    def build_model(self):
        import keras
        from keras.models import Sequential
        from keras.layers import Dense

        # Initialising the ANN
        self.classifier = Sequential()

        # Adding the input layer and the first hidden layer
        self.classifier.add(Dense(units = 12, kernel_initializer = 'uniform', activation = 'relu', input_dim = 18))

        # Adding the second hidden layer
        self.classifier.add(Dense(units = 12, kernel_initializer = 'uniform', activation = 'relu'))

        # Adding the output layer
        self.classifier.add(Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid'))

        # Compiling the ANN
        self.classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

    def train_model(self):
        from sklearn.model_selection import train_test_split
        X_train, X_test, Y_train, Y_test = train_test_split(self.X, self.y, test_size = 0.2, random_state = 0)

        # Feature Scaling
        from sklearn.preprocessing import StandardScaler
        sc = StandardScaler()
        X_train = sc.fit_transform(X_train)
        X_test = sc.transform(X_test)
##        # Encoding categorical data
##        from sklearn.preprocessing import LabelEncoder, OneHotEncoder
##        from sklearn.compose import ColumnTransformer
##        
##        #labelencoder_X_1 = LabelEncoder()
##        #self.X[:, 1] = labelencoder_X_1.fit_transform(self.X[:, 1])
##        #labelencoder_X_2 = LabelEncoder()
##        #self.X[:, 2] = labelencoder_X_2.fit_transform(self.X[:, 2])
##        #onehotencoder = OneHotEncoder(categorical_features = [1])
##        #self.X = onehotencoder.fit_transform(self.X).toarray()
##        ct = ColumnTransformer(
##            [('one_hot_encoder', OneHotEncoder(), [1])],    # The column numbers to be transformed (here is [0] but can be [0, 1, 3])
##            remainder='passthrough'                         # Leave the rest of the columns untouched
##        )
##        self.X = ct.fit_transform(self.X)
##        self.X = self.X[:, 1:]
##
##        # Splitting the dataframe into the Training set and Test set
##        from sklearn.model_selection import train_test_split
##        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X, self.y, test_size = 0.2, random_state = 0)
##
##        # Feature Scaling
##        from sklearn.preprocessing import StandardScaler
##        sc = StandardScaler(with_mean=False)
##        print(self.X_train)
##        self.X_train = sc.fit_transform(self.X_train)
##        self.X_test = sc.transform(self.X_test)
##
        self.classifier.fit(X_train, Y_train, batch_size = 10, epochs = 100)
        self.classifier.save("test.model")



    def predict(self, data):
        # Predicting the Test set results
        y_pred = self.classifier.predict(self.X_test)
        y_pred = (y_pred > 0.5)

        print(y_pred)

        # Making the Confusion Matrix
        from sklearn.metrics import confusion_matrix
        cm = confusion_matrix(self.Y_test, y_pred)

        print(cm)

        # Predicting a single new observation

        new_prediction = self.classifier.predict(sc.transform(data))
        new_prediction = (new_prediction > 0.5)

if __name__ == "__main__":
    from final_dataset_analysis import get_processed_final_dataset
    from sklearn.model_selection import train_test_split

    seed = 7
    np.random.seed(seed)

    # X, X_s, sc, y = get_processed_final_dataset()
    sdis_per_category_processed = get_processed_final_dataset()
    
    X, X_s, sc, y = sdis_per_category_processed['SUAP']

    X_train, X_test, y_train, y_test = train_test_split(X_s, y, test_size=0.33, random_state=seed)

    
    
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import Dropout
    from keras import metrics

    # Initialising the ANN
    model = Sequential()
    
    model.add(Dense(64, input_dim=14, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(32, activation='relu'))
    # model.add(Dropout(0.5))
    model.add(Dense(16, activation='relu'))
    # model.add(Dropout(0.15))
    model.add(Dense(1))
    
    # Compile model
    model.compile(optimizer ='adam', loss = 'mean_squared_error', 
              metrics =[metrics.mae])
    # model.summary()

    # wait = input()

    #history = model.fit(X_train, y_train, batch_size = 32, epochs = 200)
    history = model.fit(X_train, y_train, validation_data=(X_test,y_test), batch_size = 32, epochs = 500)
    y_pred = model.predict(X_test)
    
    for i in range(len(y_pred)):
        print('test: ' + str(y_test.iloc[i]) + ' | pred: ' + str(y_pred[i, 0]))
    
    


    # print(history.history)
    i = input()

    # # summarize history for accuracy
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    plt.title('model accuracy')
    plt.ylabel('error')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    #summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    i = input()

    
    #model = Model(X)
    #model.build_model()
    #model.train_model()
