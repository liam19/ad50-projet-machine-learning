# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 14:28:56 2020

@author: liam1
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.decomposition import PCA

from analysis.sdis91_analysis import get_processed_sdis_dataset
from analysis.epidemiological_analysis import get_processed_flu_dataset, get_processed_diarrhea_dataset, get_processed_chickenpox_dataset
from analysis.jours_ferie_analysis import get_processed_jours_ferie_dataset
from analysis.weather_analysis import get_processed_weather_dataset


def merge_datasets():
    sdis_per_category = get_processed_sdis_dataset()

    X_flu = get_processed_flu_dataset()
    X_flu = X_flu.rename(columns={'year_flu': 'ope_annee', 'weeknumber_flu': 'ope_semaine'})

    X_diarrhea = get_processed_diarrhea_dataset()
    X_diarrhea = X_diarrhea.rename(columns={'year_diarrhea': 'ope_annee', 'weeknumber_diarrhea': 'ope_semaine'})

    X_chickenpox = get_processed_chickenpox_dataset()
    X_chickenpox = X_chickenpox.rename(columns={'year_chickenpox': 'ope_annee', 'weeknumber_chickenpox': 'ope_semaine'})

    X_jours_ferie = get_processed_jours_ferie_dataset()
    X_jours_ferie = X_jours_ferie.rename(columns={'annee': 'ope_annee', 'numero_semaine': 'ope_semaine'})
    
    X_weather = get_processed_weather_dataset()
    X_weather = X_weather.rename(columns={'year': 'ope_annee', 'week': 'ope_semaine'})
    
    for category in sdis_per_category.keys():
        curr_sdis = sdis_per_category[category]
        
        curr_sdis = pd.merge(curr_sdis, X_flu, how='inner')
        curr_sdis = pd.merge(curr_sdis, X_diarrhea, how='inner')
        curr_sdis = pd.merge(curr_sdis, X_chickenpox, how='inner')
        curr_sdis = pd.merge(curr_sdis, X_jours_ferie, how='inner')
        curr_sdis = pd.merge(curr_sdis, X_weather, how='inner')
        
        # drop useless column
        curr_sdis = curr_sdis.drop(columns=['ope_annee'])
        
        sdis_per_category[category] = curr_sdis

    
    return sdis_per_category


def split_final_dataset(X):
    y = X['nb_ope']
    X = X.drop(columns=['nb_ope'])
    
    return X, y


def select_features(X, y):
    """
    Selection based on correlation between features of X and y.
    """
    corr = X.corrwith(y)
    cols_to_drop =  corr[corr <= 0].index
    X_remaining = X.drop(columns=cols_to_drop)
    return X_remaining


def do_final_dataset_preprocessing(X):
    # process ope_categorie (OneHotEncoder with ColumnTransform)
    ope_categorie_index = 3
    
    onehot_encoder = ColumnTransformer([('onehotencoder', OneHotEncoder(), [ope_categorie_index])])
    
    new_cols = onehot_encoder.fit_transform(X).toarray()
    new_col_names = ['ope_category_' + str(i) for i in range(1, new_cols.shape[1] + 1)]
    new_cols = pd.DataFrame(data=new_cols, columns=new_col_names)
    
    X = X.join(new_cols)
    X = X.drop(columns=['ope_categorie'])

    return X


def do_final_dataset_transformation(X):
    scaler = StandardScaler()
    
    X_scaled = scaler.fit_transform(X)
    
    return X_scaled, scaler


def visualize_final_dataset(X, y, n_components=2):
    pca = PCA(n_components=n_components)
    X_pca = pca.fit_transform(X)
    col_names = ['pca_' + str(i + 1) for i in range(X_pca.shape[1])]
    X_pca = pd.DataFrame(X_pca, columns=col_names)
    X_pca['y'] = y
    
    plt.figure()
    sns.scatterplot(x='pca_1', y='pca_2', hue='y', data=X_pca)
    plt.show()


def get_processed_final_dataset():
    sdis_per_category = merge_datasets()
    sdis_per_category_processed = {}
    for cat, dataset in sdis_per_category.items():
        X, y = split_final_dataset(dataset)
        #X = select_features(X, y)
        if cat == "INCN":
            X.to_csv("incn2.csv")
        X_scaled, scaler = do_final_dataset_transformation(X)
        sdis_per_category_processed[cat] = [X, X_scaled, scaler, y]
    
    return sdis_per_category_processed
