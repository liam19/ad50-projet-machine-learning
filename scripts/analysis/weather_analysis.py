# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 17:07:26 2020

@author: liam1
"""

from wwo_hist import retrieve_hist_data
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from datetime import date


def download_weather_dataset():
    location_list = ['paris']
    frequency = 24 # day
    start_date = '01-JAN-2010'
    end_date = '01-JAN-2018'
    api_key = '' # complete with your key
    
    hist_weather_data = retrieve_hist_data(
        api_key, location_list, start_date,
        end_date, frequency, location_label = False,
        export_csv = True, store_df = True
    )

def get_processed_weather_dataset():
    weather = pd.read_csv('../datasets/weather_paris.csv')
    weather = weather.drop(columns=['location', 'moonrise', 'moonset', 'sunrise', 'sunset'])
    
    # split date_time column into (year, week number)
    date_time = []
    
    for i in range(weather.shape[0]):
        d = weather.iloc[i, 0]
        (year, week_num, _) = date.fromisoformat(d).isocalendar()
        date_time.append([year, week_num])
        
        
    times_df = pd.DataFrame(date_time, columns=['year', 'week'])
        
    weather = pd.concat([weather, times_df], axis=1)
    weather = weather.drop(columns=['date_time'])
    
    # features we want to keep
    column_names = ['maxtempC', 'mintempC', 'humidity', 
                    'pressure', 'tempC', 'windspeedKmph',
                    'totalSnow_cm', 'sunHour', 'precipMM',
                    'year', 'week']
    
    # drop useless features
    weather = weather[column_names]
    
    weather_final = pd.DataFrame(columns=column_names)
    
    # mean operation will be applied on those features 
    cols_mean_op = ['maxtempC', 'mintempC', 'humidity', 'pressure', 'tempC', 'windspeedKmph']
    
    # sum operation will be applied on those features 
    cols_sum_op = ['totalSnow_cm', 'sunHour', 'precipMM']
    
    # for each couple (year, week), make mean operation on features in cols_mean_op, 
    # and sum operation on features in cols_sum_op.
    for year in weather['year'].unique():
        for week in weather['week'].unique():
            filtered = weather[(weather['year'] == year) & (weather['week'] == week)]
            
            if filtered.shape[0] == 0: 
                continue
            
            temp_mean = filtered[cols_mean_op].sum(axis=0) / filtered.shape[0]
            temp_sum = filtered[cols_sum_op].sum(axis=0)
            temp = np.concatenate([temp_mean, temp_sum, [year, week]])
            
            weather_final.loc[len(weather_final.index)] = temp
    
    
    return weather_final
