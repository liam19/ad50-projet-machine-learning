# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 18:11:05 2020

@author: liam1
"""

# =============================================================================
# IMPORTS
# =============================================================================

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import seaborn as sns


def get_processed_sdis_dataset():
    """
    Gather all the sub datasets (1 set per category) of the sdis dataset.

    Returns
    -------
    sdis_per_categ : dict
        key -> category (str)
        value -> dataset (dataframe).

    """    
    filepath_sdis = '../datasets/interventions-hebdo-2010-2017.csv'
    sdis = collect_sdis_dataset(filepath_sdis)
    sdis = select_sdis_features(sdis)
    
    sdis_per_categ = build_sdis_sub_datasets(sdis)
    
    return sdis_per_categ


def collect_sdis_dataset(filepath):
    """
    Collect the sdis dataset and process it a little.

    Parameters
    ----------
    filepath : str
        Path to the dataset.

    Returns
    -------
    sdis_dataset : DataFrame.

    """
    
    sdis_dataset = pd.read_csv(filepath, sep=';')
    sdis_dataset['ope_categorie'] = sdis_dataset['ope_categorie'].fillna('AUTR')
    sdis_dataset = sdis_dataset.dropna()

    return sdis_dataset


def select_sdis_features(sdis):
    sdis = sdis.drop(columns=['ope_code_postal', 'ope_nom_commune'])
    return sdis
    

def build_sdis_sub_datasets(X):
    """
    Split X into 5 datasets, one per category.

    Parameters
    ----------
    X : DataFrame

    Returns
    -------
    sdis_per_categ : dict
        key -> category (str)
        value -> dataset (dataframe).

    """
    sdis_per_categ = {}
    
    for category in X['ope_categorie'].unique():
        sdis_per_categ[category] = build_data(X, category)
        
    return sdis_per_categ


def build_data(X, category):
    """
    Build from X a dataset only related to category.

    Parameters
    ----------
    X : DataFrame

    category : str

    Returns
    -------
    X_res : DataFrame
        The dataset only related to category.

    """
    X_temp = X.loc[X['ope_categorie'] == category].drop(columns=['ope_categorie']) # filter by category
    # X_res = pd.DataFrame(columns=['ope_annee', 'ope_semaine', 'nb_ope'])
    
    # # for each pair year / weak, compute the # of operations and insert that in a new DataFrame
    # for year in X_temp['ope_annee'].unique():
    #     for weak in X_temp['ope_semaine'].unique():
    #         temp = X_temp.loc[(X_temp['ope_annee'] == year) & (X_temp['ope_semaine'] == weak)]
    #         nb_ope = float(temp['nb_ope'].sum(axis=0))
    
    #         X_res.loc[len(X_res)] = [year, weak, nb_ope]
            
    
    # # for weak in X_temp['ope_semaine'].unique():
    # #     nb_ope = 0
    # #     temp_weak = X_temp.loc[X_temp['ope_semaine'] == weak]
        
    # #     for year in temp_weak['ope_annee'].unique():
    # #         temp_year = temp_weak.loc[X_temp['ope_annee'] == year]
    # #         nb_ope += float(temp_year['nb_ope'].sum(axis=0))
        
    # #     nb_ope /= len(temp_weak['ope_annee'].unique())
        
    # #     X_res.loc[len(X_res)] = [year, weak, nb_ope]
    
    # return X_res
    return X_temp


def visualize_full_sdis_dataset(sdis_dataset):
    # total ope / year 
    total_ope_per_year = {}
    
    for year in sdis_dataset['ope_annee'].unique():
        sdis_filtered = sdis_dataset[(sdis_dataset['ope_annee'] == year)]
        total_ope_per_year[str(year)] = sdis_filtered['nb_ope'].sum()
    
    plt.figure()
    plt.bar(list(total_ope_per_year.keys()), total_ope_per_year.values())
    plt.title('Nombre total opérations par année (toute commune confondue)')
    plt.xlabel('années')
    plt.ylabel('# opérations')
    plt.show()

    # total ope / week in 2015 
    total_ope_per_week = {}
    sdis_filtered = sdis_dataset[(sdis_dataset['ope_annee'] == 2015)]
    for week in sdis_dataset['ope_semaine'].unique():
        sdis_filtered = sdis_dataset[(sdis_dataset['ope_semaine'] == week)]
        total_ope_per_week[str(week)] = sdis_filtered['nb_ope'].sum()
    
    plt.figure()
    plt.bar(list(total_ope_per_week.keys()), total_ope_per_week.values())
    plt.title('Nombre total opérations par semaine en 2015 (toute commune confondue)')
    plt.xlabel('semaines')
    plt.xticks([])
    plt.ylabel('# opérations')
    plt.show()
         
    # total ope / categorie
    total_ope_per_cat = {}
    
    for cat in sdis_dataset['ope_categorie'].dropna().unique(): # drop lines where cat is nan
        sdis_filtered = sdis_dataset[(sdis_dataset['ope_categorie'] == cat)]
        total_ope_per_cat[cat] = sdis_filtered['nb_ope'].sum()
        
    plt.bar(list(total_ope_per_cat.keys()), total_ope_per_cat.values())
    plt.title('Nombre total opérations par catégorie (toute commune / année confondue)')
    plt.xlabel('catégories')
    plt.ylabel('# operations')
    plt.show()
