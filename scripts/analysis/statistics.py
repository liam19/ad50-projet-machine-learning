import pandas as pd
import numpy as np


def get_statistics(data):
    if data == 'rain':
        return [0, 0.5, 2.2, 4.5, 8, 15, 100]
    elif data == 'snow':
        return [0, 0.3, 1.5, 6, 200]
    elif data == 'sun':
        return [35, 55, 75, 90, 168]
    elif data == 'wind':
        return [8, 10, 12.5, 18, 100]
    else:
        return -1
        
        
if __name__ == "__main__":
    weather = pd.read_csv("../../datasets/weather_paris.csv")
    rain = weather["precipMM"].to_numpy()
    snow = weather["totalSnow_cm"].to_numpy()
    sun = weather["sunHour"].to_numpy()
    wind = weather['windspeedKmph'].to_numpy()


    rainM = []
    for i in range(0, len(rain) - 7, 7):
        rainM.append(np.sum(rain[i:i+7]))
    rainM.sort()
    resR = []
    resS = []
    resW = []
    nb = 6

    snowM = []
    for i in range(0, len(snow) - 7, 7):
        snowM.append(np.sum(snow[i:i + 7]))
    snowM.sort()

    sunM = []
    for i in range(0, len(sun) - 7, 7):
        sunM.append(np.sum(sun[i:i+7]))
    sunM.sort()

    windM = []
    for i in range(0, len(wind) - 7, 7):
        windM.append(np.sum(wind[i:i+7]) / 7)
    windM.sort()
    print(windM)

    for i in range(1,nb):
        resR.append(rainM[int(i * (len(rainM) / nb))])

    for i in range(1,5):
        resS.append(sunM[int(i * (len(sunM) / 5))])

    for i in range(1,5):
        resW.append(windM[int(i * (len(windM) / 5))])

    print("rain :")
    print(resR)
    print("sun : ")
    print(resS)
    print("wind :")
    print(resW)


            
    
